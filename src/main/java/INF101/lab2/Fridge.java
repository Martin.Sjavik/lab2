package INF101.lab2;


import java.util.ArrayList;
import java.util.NoSuchElementException;


public class Fridge implements IFridge{

    ArrayList<FridgeItem> items;
    int maxCap;


    public Fridge() {
        this.items = new ArrayList<FridgeItem>();
        this.maxCap = 20;
    }

    public int nItemsInFridge() {
        return this.items.size();
        }
    
    public int totalSize() {
        return this.maxCap;
    }
    
    public boolean placeIn(FridgeItem item) {
        if (this.totalSize() - this.nItemsInFridge() >= 1) {
            this.items.add(item);
            return true;
        } else {
            return false;
        }
    }

    public void takeOut(FridgeItem item) {

        if (this.items.contains(item)) {
            this.items.remove(item);
        } else {
            throw new NoSuchElementException("Item not in Fridge");
        }
    }

    public void emptyFridge() {
        items.clear();
    }
    
    public ArrayList<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).hasExpired()) {
                expiredItems.add(items.get(i));
                
            } else {
                continue;
            }
        }
        for (int i = 0; i < expiredItems.size(); i++) {
            if (items.contains(expiredItems.get(i))) {
                items.remove(expiredItems.get(i));
            } else {
                continue;
            }
        }

        return expiredItems;
    }
}
